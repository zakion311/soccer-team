<?php
/*
Plugin Name: Soccer Team
*/
require "vendor/autoload.php";

use App\Controller\ControllerCompetition;
use App\Menu;

class SoccerTeam {
    
    public function __construct()
    {
       
        register_activation_hook(__FILE__,array($this,'install'));
        register_deactivation_hook(__FILE__,array('SoccerTeam', 'uninstall'));
        $adminMenu = new Menu();
        
    }
   

   
    public function install(){

        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();
        var_dump($wpdb);
        $sql = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}st_calendar` (
            id INT NOT NULL AUTO_INCREMENT,
            create_at DATETIME NOT NULL,
            update_at DATETIME NOT NULL,
            PRIMARY KEY (id))$charset_collate;";   
       $wpdb->query($sql);

       $sql = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}st_competition` (
        `id` INT NOT NULL AUTO_INCREMENT,
        `name` VARCHAR(100) NOT NULL,
        `date_begin` DATETIME NOT NULL,
        `date_end` DATETIME NOT NULL,
        `create_at` DATETIME NOT NULL,
        `update_at` DATETIME NOT NULL,
        PRIMARY KEY (`id`))$charset_collate;";
        $wpdb->query($sql);

       $sql = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}st_match` (
        `id` INT NOT NULL AUTO_INCREMENT,
        `id_calendar` INT NOT NULL,
        `id_competition` INT NOT NULL,
        `day` DATETIME NOT NULL,
        `create_at` DATETIME NULL,
        `update_at` DATETIME NULL,
        PRIMARY KEY (`id`, `id_calendar`, `id_competition`),
        INDEX `fk_Match_Calendar_idx` (`id_calendar` ASC),
        INDEX `fk_Match_Competition1_idx` (`id_competition` ASC),
        CONSTRAINT `fk_Match_Calendar`
            FOREIGN KEY (`id_calendar`)
            REFERENCES `mydb`.`Calendar` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
        CONSTRAINT `fk_Match_Competition1`
            FOREIGN KEY (`id_competition`)
            REFERENCES `mydb`.`Competition` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION)$charset_collate;";
        $wpdb->query($sql);

        $sql = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}st_team` (
            `id` INT NOT NULL AUTO_INCREMENT,
            `id_calendar` INT NOT NULL,
            `create_at` DATETIME NOT NULL,
            `update_at` DATETIME NOT NULL,
            PRIMARY KEY (`id`, `id_calendar`),
            INDEX `fk_Team_Calendar1_idx` (`id_calendar` ASC),
            CONSTRAINT `fk_Team_Calendar1`
                FOREIGN KEY (`id_calendar`)
                REFERENCES `mydb`.`Calendar` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)$charset_collate;";
        $wpdb->query($sql);
        
        $sql = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}st_player` (
            `id` INT NOT NULL AUTO_INCREMENT,
            `id_team` INT NOT NULL,
            `first_name` VARCHAR(45) NULL,
            `create_at` DATETIME NULL,
            `update_at` DATETIME NULL,
            PRIMARY KEY (`id`, `id_team`),
            INDEX `fk_Player_Team1_idx` (`id_team` ASC),
            CONSTRAINT `fk_Player_Team1`
                FOREIGN KEY (`id_team`)
                REFERENCES `mydb`.`Team` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            $charset_collate;";
        $wpdb->query($sql);

        
    }

    public static function uninstall(){
        global $wpdb;

        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}Team;
        DROP TABLE IF EXISTS {$wpdb->prefix}Player;
        DROP TABLE IF EXISTS {$wpdb->prefix}Calendar;
        DROP TABLE IF EXISTS {$wpdb->prefix}Match;
        DROP TABLE IF EXISTS {$wpdb->prefix}Competition;");
    }
}
new SoccerTeam();
?>