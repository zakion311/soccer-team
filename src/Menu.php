<?php 
/**
 * 
 * @package soccer-team
 * @subpackage soccer-team/View/Admin
 */
namespace App;

use App\Controller\ControllerCompetition;

class Menu {

    public function __construct()
    {
        $this->run();
        $controllerCompetition = new ControllerCompetition();
        $controllerCompetition->initMenu();
    }

    public function run(){
        add_action('admin_menu', array($this, 'createMenu'));
        
        
    }

    public function createMenu(){
        
        

        add_menu_page('Notre premier plugin', 'Soccer Team', 'manage_options', 'soccer', array($this, 'menu_html'));
        

    }

    public function menu_html()
    {
        echo '<h1>'.get_admin_page_title().'</h1>';
        echo '<p>Bienvenue sur la page d\'accueil du plugin</p>';
    }
}



?>