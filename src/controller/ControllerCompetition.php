<?php

namespace App\Controller;
class ControllerCompetition{

    public function __construct()
    {
       
    }

    public function doList(){
        include ABSPATH . "wp-content/plugins/soccer-team/src/view/competition/list.php";
    }

    public function doNew(){
        include ABSPATH . "wp-content/plugins/soccer-team/src/view/competition/new.php";
    }

    public function doCreate(string $str){
        if(empty($str)){
            include ABSPATH . "wp-content/plugins/soccer-team/src/view/competition/list.php";
        }else{
            include ABSPATH . "wp-content/plugins/soccer-team/src/view/competition/new.php";
        }
        
    }

    public function initMenu(){
        
        add_action('admin_menu',array($this,'menu'),20);
    }
    
    public function menu(){
        add_submenu_page("soccer","Compétition","Compétition","manage_options","Compétition",array($this,"doList"));
        add_submenu_page("soccer","Nouveau", "nouveau","manage_options","nouveau", array($this,"doNew"));
    }


}


?>